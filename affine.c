#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

bool eligible_key(int a, int b);
bool inverse_key(int a, int *out);
void affine_encrypt(FILE *out, FILE *in, int a, int b);
void affine_decrypt(FILE *out, FILE *in, int a, int b);

int main(int argc, char **argv) {
	if (argc != 4 || (
		strcmp(argv[1], "check") &&
		strcmp(argv[1], "encrypt") &&
		strcmp(argv[1], "decrypt")
	)) {
		fputs(
			"Usage: affine check|encrypt|decrypt "
			"[multiply] [add]\n",
			stderr
		);
		return 1;
	}
	int a, b;
	a = atoi(argv[2]);
	b = atoi(argv[3]);
	if (!strcmp(argv[1], "check")) {
		fprintf(
			stderr,
			"The key is %seligible.\n",
			eligible_key(a, b) ? "" : "NOT "
		);
		return eligible_key(a, b) ? 0 : 2;
	} else if (!strcmp(argv[1], "encrypt")) {
		affine_encrypt(stdout, stdin, a, b);
	} else if (!strcmp(argv[1], "decrypt")) {
		affine_decrypt(stdout, stdin, a, b);
	}
	return 0;
}

bool eligible_key(int a, int b) {
	return
		(a > 1) && (a < 26) &&
		(b >= 0) && (b < 26) &&
		(a % 2 != 0) && (a % 13 != 0);
}

bool inverse_key(int a, int *out) {
	for (int i = 1; i < 26; i++) {
		if ((i * a) % 26 == 1) {
			*out = i;
			return true;
		}
	}
	return false;
}

void affine_encrypt(FILE *out, FILE *in, int a, int b) {
	int c;
	while ((c = fgetc(in)) != EOF) {
		if (c >= 'a' && c <= 'z')
			c = ((c - 'a') * a + b) % 26 + 'a';
		if (c >= 'A' && c <= 'Z')
			c = ((c - 'A') * a + b) % 26 + 'A';
		fputc(c, out);
	}
}

void affine_decrypt(FILE *out, FILE *in, int a, int b) {
	int c;
	inverse_key(a, &a);
	while ((c = fgetc(in)) != EOF) {
		if (c >= 'a' && c <= 'z')
			c = ((c - b - 'a') * a % 26 + 26) % 26 + 'a';
		if (c >= 'A' && c <= 'Z')
			c = ((c - b - 'A') * a % 26 + 26) % 26 + 'A';
		fputc(c, out);
	}
}
