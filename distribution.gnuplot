set terminal png size 1000,600
set boxwidth 0.5
set style fill solid
unset key
stats 'distribution.txt' using 2
plot 'distribution.txt' using 0:2:xtic(1) with boxes, \
     'distribution.txt' using 0:($2+2):2 with labels, \
     'distribution.txt' using \
         0:($2+4):(gprintf("%.1f%%", $2 / STATS_sum * 100)) with labels
