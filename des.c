#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

extern const size_t table_IP[64];
extern const size_t table_IIP[64];
extern const size_t table_E[48];
extern const uint8_t table_S[8][4][16];
extern const size_t table_P[32];
extern const size_t table_PC1[56];
extern const size_t table_PC2[48];
extern const size_t table_shift[16];

uint64_t read_key(FILE *in);
void des_encrypt_ecb(FILE *out, FILE *in, uint64_t key);
void des_decrypt_ecb(FILE *out, FILE *in, uint64_t key);
uint64_t des_encrypt(uint64_t block, uint64_t key);
uint64_t des_decrypt(uint64_t block, uint64_t key);
uint32_t f(uint32_t R, uint64_t K);
uint64_t KS(int n, uint64_t key);
uint8_t S(size_t i, uint8_t sextet);
uint64_t pad_pkcs5(uint64_t block, size_t octets);
size_t unpad_pkcs5(uint64_t *out, uint64_t in);
bool block_read(uint64_t *out, FILE *in, size_t *octets);
void block_write(FILE *out, uint64_t in, size_t octets);
uint64_t shift(uint64_t in, size_t delta, size_t from);
uint64_t permute(
	uint64_t in,
	const size_t table[],
	size_t to,
	size_t from
);
uint64_t low(uint64_t in, size_t to);
uint64_t high(uint64_t in, size_t to, size_t from);

int main(int argc, char **argv) {
	if (argc != 3 || (
		strcmp(argv[1], "encrypt") &&
		strcmp(argv[1], "decrypt")
	)) {
		fputs(
			"Usage: des encrypt|decrypt [key file]\n",
			stderr
		);
		return 1;
	}
	FILE *file = fopen(argv[2], "rb");
	if (!file) {
		perror(argv[2]);
		return 2;
	}
	uint64_t key = read_key(file);
	if (!strcmp(argv[1], "encrypt"))
		des_encrypt_ecb(stdout, stdin, key);
	else if (!strcmp(argv[1], "decrypt"))
		des_decrypt_ecb(stdout, stdin, key);
	fclose(file);
	return 0;
}

uint64_t read_key(FILE *in) {
	uint64_t key;
	size_t octets;
	block_read(&key, in, &octets);
	return pad_pkcs5(key, octets);
}

void des_encrypt_ecb(FILE *out, FILE *in, uint64_t key) {
	size_t size;
	uint64_t block;
	while (block_read(&block, in, &size)) {
		block = pad_pkcs5(block, size);
		block = des_encrypt(block, key);
		block_write(out, block, 8);
	}
}

void des_decrypt_ecb(FILE *out, FILE *in, uint64_t key) {
	size_t size = 0;
	uint64_t block = 0, previous = 0;
	bool previous_exists = false;
	while (block_read(&block, in, &size) && size > 0) {
		if (previous_exists) {
			previous = des_decrypt(previous, key);
			block_write(out, previous, size);
		}
		previous_exists = true;
		previous = block;
	}
	previous = des_decrypt(previous, key);
	size = unpad_pkcs5(&previous, previous);
	block_write(out, previous, size);
}

uint64_t des_encrypt(uint64_t block, uint64_t key) {
	block = permute(block, table_IP, 64, 64);
	for (int i = 1; i <= 16; i++) {
		uint32_t old_L = (uint32_t) high(block, 32, 64);
		uint32_t old_R = (uint32_t) low(block, 32);
		uint64_t new_L = old_R;
		uint64_t new_R = old_L ^ f(old_R, KS(i, key));
		block = (new_L << 32) | new_R;
	}
	block = (low(block, 32) << 32) | high(block, 32, 64);
	return permute(block, table_IIP, 64, 64);
}

uint64_t des_decrypt(uint64_t block, uint64_t key) {
	block = permute(block, table_IP, 64, 64);
	for (int i = 16; i >= 1; i--) {
		uint32_t old_L = (uint32_t) high(block, 32, 64);
		uint32_t old_R = (uint32_t) low(block, 32);
		uint64_t new_L = old_R;
		uint64_t new_R = old_L ^ f(old_R, KS(i, key));
		block = (new_L << 32) | new_R;
	}
	block = (low(block, 32) << 32) | high(block, 32, 64);
	return permute(block, table_IIP, 64, 64);
}

uint32_t f(uint32_t R, uint64_t K) {
	uint64_t expanded = permute(R, table_E, 48, 32);
	uint64_t selector = expanded ^ K;
	uint32_t out = 0;
	for (size_t i = 1; i <= 8; i++) {
		uint8_t sextet = (uint8_t)
			low(high(selector, 6 * i, 48), 6);
		out <<= 4;
		out |= S(i, sextet);
	}
	return (uint32_t) permute(out, table_P, 32, 32);
}

uint64_t KS(int n, uint64_t key) {
	key = permute(key, table_PC1, 56, 64);
	while (n--) {
		uint64_t C = high(key, 28, 56);
		uint64_t D = low(key, 28);
		C = shift(C, table_shift[n], 28);
		D = shift(D, table_shift[n], 28);
		key = (C << 28) | D;
	}
	return permute(key, table_PC2, 48, 56);
}

uint8_t S(size_t i, uint8_t sextet) {
	int row = ((sextet >> 5) << 1) | (sextet & 1);
	int column = (sextet >> 1) & ((1 << 4) - 1);
	return table_S[i - 1][row][column];
}

uint64_t pad_pkcs5(uint64_t block, size_t octets) {
	for (size_t i = octets; i < 8; i++)
		block = (block << 8) | (uint8_t) (8 - octets);
	return block;
}

size_t unpad_pkcs5(uint64_t *out, uint64_t in) {
	*out = in >> (8 * low(in, 8));
	return 8 - low(in, 8);
}

bool block_read(uint64_t *out, FILE *in, size_t *octets) {
	uint64_t temp = 0;
	int octet;
	size_t i;
	if (feof(in))
		return false;
	for (i = 0; i < 8; i++)
		if ((octet = fgetc(in)) != EOF)
			temp = (temp << 8) | (uint8_t) octet;
		else break;
	*out = temp;
	*octets = i;
	return true;
}

void block_write(FILE *out, uint64_t in, size_t octets) {
	for (size_t i = 1; i <= octets; i++) {
		uint8_t octet = (uint8_t)
			low(high(in, 8 * i, 8 * octets), 8);
		fputc(octet, out);
	}
}

uint64_t shift(uint64_t in, size_t delta, size_t from) {
	return (low(in, from - delta) << delta) |
		high(in, delta, from);
}

uint64_t permute(
	uint64_t in,
	const size_t table[],
	size_t to,
	size_t from
) {
	uint64_t out = 0;
	for (size_t i = 0; i < to; i++) {
		bool bit = (in >> (from - table[i])) & 1;
		out |= (uint64_t) bit << (to - i - 1);
	}
	return out;
}

uint64_t low(uint64_t in, size_t to) {
	if (to == 64)
		return in;
	return in & ((1ULL << to) - 1);
}

uint64_t high(uint64_t in, size_t to, size_t from) {
	if (from - to == 64)
		return in;
	else if (to > from)
		to = from;
	return low(in >> (from - to), to);
}

const size_t table_IP[64] = {
	58, 50, 42, 34, 26, 18, 10, 2,
	60, 52, 44, 36, 28, 20, 12, 4,
	62, 54, 46, 38, 30, 22, 14, 6,
	64, 56, 48, 40, 32, 24, 16, 8,
	57, 49, 41, 33, 25, 17, 9, 1,
	59, 51, 43, 35, 27, 19, 11, 3,
	61, 53, 45, 37, 29, 21, 13, 5,
	63, 55, 47, 39, 31, 23, 15, 7
};

const size_t table_IIP[64] = {
	40, 8, 48, 16, 56, 24, 64, 32,
	39, 7, 47, 15, 55, 23, 63, 31,
	38, 6, 46, 14, 54, 22, 62, 30,
	37, 5, 45, 13, 53, 21, 61, 29,
	36, 4, 44, 12, 52, 20, 60, 28,
	35, 3, 43, 11, 51, 19, 59, 27,
	34, 2, 42, 10, 50, 18, 58, 26,
	33, 1, 41, 9, 49, 17, 57, 25
};

const size_t table_E[48] = {
	32, 1, 2, 3, 4, 5,
	4, 5, 6, 7, 8, 9,
	8, 9, 10, 11, 12, 13,
	12, 13, 14, 15, 16, 17,
	16, 17, 18, 19, 20, 21,
	20, 21, 22, 23, 24, 25,
	24, 25, 26, 27, 28, 29,
	28, 29, 30, 31, 32, 1
};

const uint8_t table_S[8][4][16] = {
	{{14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7},
	 {0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8},
	 {4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0},
	 {15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13}},
	{{15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10},
	 {3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5},
	 {0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15},
	 {13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9}},
	{{10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8},
	 {13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1},
	 {13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7},
	 {1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12}},
	{{7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15},
	 {13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9},
	 {10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4},
	 {3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14}},
	{{2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9},
	 {14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6},
	 {4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14},
	 {11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3}},
	{{12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11},
	 {10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8},
	 {9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6},
	 {4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13}},
	{{4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1},
	 {13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6},
	 {1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2},
	 {6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12}},
	{{13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7},
	 {1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2},
	 {7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8},
	 {2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11}}
};

const size_t table_P[32] = {
	16, 7, 20, 21,
	29, 12, 28, 17,
	1, 15, 23, 26,
	5, 18, 31, 10,
	2, 8, 24, 14,
	32, 27, 3, 9,
	19, 13, 30, 6,
	22, 11, 4, 25
};

const size_t table_PC1[56] = {
	57, 49, 41, 33, 25, 17, 9,
	1, 58, 50, 42, 34, 26, 18,
	10, 2, 59, 51, 43, 35, 27,
	19, 11, 3, 60, 52, 44, 36,
	63, 55, 47, 39, 31, 23, 15,
	7, 62, 54, 46, 38, 30, 22,
	14, 6, 61, 53, 45, 37, 29,
	21, 13, 5, 28, 20, 12, 4
};

const size_t table_PC2[48] = {
	14, 17, 11, 24, 1, 5,
	3, 28, 15, 6, 21, 10,
	23, 19, 12, 4, 26, 8,
	16, 7, 27, 20, 13, 2,
	41, 52, 31, 37, 47, 55,
	30, 40, 51, 45, 33, 48,
	44, 49, 39, 56, 34, 53,
	46, 42, 50, 36, 29, 32
};

const size_t table_shift[16] = {
	1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};
