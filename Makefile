CC = clang
CFLAGS = -std=c99 -g -Wall -Wextra -Weverything -pedantic \
         -Werror -pedantic-errors

all: des affine report.pdf distribution.png

des: des.c

affine: affine.c

report.pdf: report.tex distribution.png affine.c.tex des.c.tex \
	test0.e.out.tex test0.d.out.tex test1.e.out.tex test1.d.out.tex
	pdflatex report.tex
	pdflatex report.tex

affine.c.tex: affine.c
	source-highlight --failsafe -f latex affine.c

des.c.tex: des.c
	source-highlight --failsafe -f latex des.c

test0.e.out.tex: test0.e.out
	source-highlight --failsafe -f latex test0.e.out

test0.d.out.tex: test0.d.out
	source-highlight --failsafe -f latex test0.d.out

test1.e.out.tex: test1.e.out
	source-highlight --failsafe -f latex test1.e.out

test1.d.out.tex: test1.d.out
	source-highlight --failsafe -f latex test1.d.out

test0.e.out: test0 affine
	cat test0 | ./affine encrypt 5 8 | \
		hexdump -C > test0.e.out

test0.d.out: test0.e.out affine
	cat test0 | ./affine encrypt 5 8 | \
		./affine decrypt 5 8 | hexdump -C > test0.d.out

test1.e.out: test1 des
	rm -f test1.e.out
	cat test1 | ./des encrypt key2 | \
		hexdump -C | head -n 45 >> test1.e.out
	echo '(some data omitted)' >> test1.e.out
	cat test1 | ./des encrypt key2 | \
		hexdump -C | tail -n 45 >> test1.e.out

test1.d.out: test1 des
	rm -f test1.d.out
	cat test1 | ./des encrypt key2 | \
		./des decrypt key2 | hexdump -C | \
		head -n 45 > test1.d.out
	echo '(some data omitted)' >> test1.d.out
	cat test1 | ./des encrypt key2 | \
		./des decrypt key2 | hexdump -C | \
		tail -n 45 >> test1.d.out

distribution.png: distribution.txt
	gnuplot distribution.gnuplot > distribution.png

distribution.txt: test0
	cat test0 | tr A-Z a-z | tr -dC a-z | \
		sed 's/^/abcdefghijklmnopqrstuvwxyz/' | \
		awk -F '' -f distribution.awk | \
		tr ' ' '\t' | sort > distribution.txt
dist:
	rm -f des affine *.o *.exe* *.aux *.log *.out *.toc *.txt 
	rm -f *.e.out *.d.out *.out.tex *.c.tex

clean: dist
	rm -f *.png *.pdf
